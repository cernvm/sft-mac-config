Configuration of SFT Mac nodes
==============================

Current inventory:

|    Name    | macOS version | Location
|------------|---------------|---------
| macitois17 | 10.11         | IT
| macitois19 | 10.12         | IT
| macitois21 | 10.14         | IT
| macitois22 | 10.13         | IT
| macphsft08 | 10.13         | SFT
| macphsft16 | 10.12         | SFT
| macphsft17 | 10.14         | SFT
| macphsft18 | 10.14         | SFT
| macphsft19 | 10.14         | SFT


## How to add a new Mac node to Jenkins (manual instructions)

* Install Xcode Command Line Tools - if latest version is not compatible with the OS then you need to use a developer account and get a previous version from https://developer.apple.com.

  Install command line tools:
  ```bash
  $ xcode-select --install
  ```
  Accept license:
  ```bash
  $ sudo xcodebuild -license
  ```

  Install PackageMaker:
  ```bash
  $ curl -O http://ecsft.cern.ch/dist/mac/xcode44auxtools6938114a.dmg
  $ hdiutil attach xcode44auxtools6938114a.dmg
  $ sudo cp -r /Volumes/Auxiliary\ Tools/PackageMaker.app /Applications/
  $ hdiutil detach /Volumes/Auxiliary\ Tools/
  ```

* Install Homebrew:
  ```bash
  $ /usr/bin/ruby -e "$(curl -fsSL https://raw.githubusercontent.com/Homebrew/install/master/install)"
  ```

* Homebrew packages: CMake, Ccache, Ninja, GSL
  ```bash
  $ brew install cmake ccache ninja gsl
  ```

* Homebrew casks (binary packages): Java JDK, gfortran, OSXFUSE, XQuartz, Firefox
  ```bash
  $ brew cask install java gfortran osxfuse xquartz firefox
  ```

* Downaload CernVM-FS from https://cernvm.cern.ch/portal/filesystem/downloads

  Copy [default.local](https://gitlab.cern.ch/rpopescu/sft-mac-config/raw/master/default.local) to `/etc/cvmfs/`.

  Copy [cvmfs_check.sh](https://gitlab.cern.ch/rpopescu/sft-mac-config/raw/master/cvmfs_check.sh) to `/usr/local/bin/` and make it executable.

  Copy [cvmfs.filesystems.plist](https://gitlab.cern.ch/rpopescu/sft-mac-config/raw/master/cvmfs.filesystems.plist) to `/Library/LaunchDaemons` and reboot the machine to mount all the CernVM-FS volumes.

* Set up Jenkins workspace

  On older machines, with two disks, rename the volumes HD1 and HD2, then create `/Volumes/HD2/build`:
  ```bash
  $ mkdir /Volumes/HD2/build
  ```
  create the link `/build -> /Volumes/HD2/build`:
  ```bash
  $ sudo ln -s /Volumes/HD2/build /build
  ```
  create `/build/jenkins` as user `sftnight`:
  ```bash
  $ cd /build
  $ mkdir jenkins
  $ sudo chown -R sftnight jenkins
  ```

  On newer machines with a single internal disk:
  ```bash
  $ mkdir -p /build/jenkins
  $ sudo chown -R sftnight /build/jenkins
  ```

* Add new Jenkins nodes

  ​Open Jenkins web interface: https://epsft-jenkins.cern.ch

  Log in on the node as `sftnight` and run the command:
  ```bash
  $ ssh-keygen -t rsa
  ```
  to create the file `id_rsa.pub` as described in https://docs.joyent.com/public-cloud/getting-started/ssh-keys/generating-an-ssh-key-manually/manually-generating-your-ssh-key-in-mac-os-x

  ​Log in on node `epsft-jenkins` as `sftnight` and copy the previous file content into `.ssh/authorized_keys`:
  ```bash
  $ cd .ssh
  $ scp sftnight@macitois17:.ssh/id_rsa.pub ./id_rsa.pub.macitois17
  $ cat id_rsa.pub.macitois17 >> authorized_keys
  $ rm id_rsa.pub.macitois17
  ```

  If this does not work copy the file content `/etc/ssh/ssh_host_rsa_key.pub` as SSH key into Jenkins node config of the new node.

  "Host Key Verification Strategy" should be set to "Manually provided key Verification Strategy".

  Login on the new mac node and copy file `/var/lib/jenkins/.ssh/id_rsa.pub` from `epsft-jenkins` into `sftnight@macitois17:~/.ssh/authorized_keys` to allow `sftnight@epsft-jenkins` to log in as `sftnight@macitois17`.

  Log in on https://epsft-jenkins.cern.ch, then:

  * Go to "Manage Jenkins -> Manage Nodes".
  * Select "New node" and copy the configuration form an existing node.
  * Verify the configuration of the new node: description and label should correspond to the OS version, change the host name with the name of the node, add the notification when the node online status changes adding the relevant email.
  * Launch slave agent
