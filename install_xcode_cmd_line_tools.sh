#!/bin/sh

xcode_url=$1

curl -o /tmp/tools.dmg ${xcode_url}

hdiutil attach /tmp/tools.dmg
installer -pkg /Volumes/Command\ Line\ Developer\ Tools/*.pkg -target /
hdiutil detach /Volumes/Command\ Line\ Developer\ Tools